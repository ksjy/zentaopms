<?php
$lang->pipeline->id       = 'ID';
$lang->pipeline->name     = 'Server Name';
$lang->pipeline->url      = 'Server URL';
$lang->pipeline->token    = 'Token';
$lang->pipeline->account  = 'Username';
$lang->pipeline->password = 'Password';
